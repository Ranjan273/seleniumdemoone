package automationDemo;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.Certificate;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Firstdemo {
	
	@Test
	public void launchapp() throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		WebDriver driver=new ChromeDriver();
		driver.get("https://www.google.com/");
		
		List<WebElement> elements=driver.findElements(By.tagName("a"));
		System.out.println("Number of available links "+elements.size());
		
		/*
		 * for(WebElement ele : elements) {
		 * 
		 * String element=ele.getText();
		 * 
		 * System.out.println("link name is"+element); }
		 */
		
		for(int i=0;i<elements.size();i++) {
			WebElement link=elements.get(i);
			System.out.println("Link is "+link);
			}
		
		Thread.sleep(5000);
		driver.quit();
	}
	
	@Test
	public void attributeGet() {
		WebDriverManager.chromedriver().setup();
		WebDriver driver=new ChromeDriver();
		Firstdemo fd=new Firstdemo();
		driver.get("https://www.google.com/");
		
	    List<WebElement> links= driver.findElements(By.tagName("a"));
	    
	    System.out.println("Total links are "+links.size());
	    
	    for(int i=0;i<links.size();i++) {
	    	
	    	WebElement ele=links.get(i);
	    	String url=ele.getAttribute("href");
	    	
	    	fd.verifyLinkActive(url);
	    }    
	}
	
	@Test
	public void verifyLinkActive(String linkurl)  {
		
		
		try
		{
			URL url=new URL(linkurl);
			
			HttpsURLConnection httpurlconection=(HttpsURLConnection)url.openConnection();
			httpurlconection.setConnectTimeout(3000);
			httpurlconection.connect();
			
			if(httpurlconection.getResponseCode()==200) {
				System.out.println(linkurl+" - "+httpurlconection.getResponseMessage());
			}
			if(httpurlconection.getResponseCode()==httpurlconection.HTTP_NOT_FOUND) {
				
				System.out.println(linkurl+ " - "+httpurlconection.getResponseMessage()+" - "+httpurlconection.HTTP_NOT_FOUND);
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	    }

}
