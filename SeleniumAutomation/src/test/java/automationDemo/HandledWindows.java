package automationDemo;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class HandledWindows {
	
	@Test
	public void handledwinddowone() throws InterruptedException {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver=new ChromeDriver();
		
		driver.get("https://www.naukri.com/");
		
		String parentid=driver.getWindowHandle();
		System.out.println(parentid);
		
		Thread.sleep(10000);
		
		driver.findElement(By.xpath("//div[@class='chip-heading-div']//*[text()='Remote']")).click();
		driver.findElement(By.xpath("//div[@class='chip-heading-div']//*[text()='MNC']")).click();
		//driver.findElement(By.xpath("//div[@data-name='L&T Infotech (LTI)']//div[4]")).click();
		
		Set<String> childids=driver.getWindowHandles();
		
		int totalwindows=childids.size();
		
		System.out.println("Total window count :" +totalwindows);
		
		for(String child : childids) {
			
			if(!parentid.contentEquals(child)) {
				driver.switchTo().window(child);
				System.out.println("Child window is :"+driver.getTitle());
				Thread.sleep(3000);
				driver.close();
			}
			
		}
		
		driver.switchTo().window(parentid);
		System.out.println("Parent window is :"+driver.getTitle());
	
		
		//driver.quit();
		
	}

}
